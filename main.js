<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-orange.min.css" /> 
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>

<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">New Trip </span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation -->
      <nav class="mdl-navigation">
        <!-- Colored raised button -->
         <!-- Raised button -->
          <button class="mdl-button mdl-js-button mdl-button--raised">
           BACK
          </button>

      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">NAVIGATION MENU</span>
    <nav class="mdl-navigation">
      
      <a class="mdl-navigation__link" href="">View Trip</a>
      <a class="mdl-navigation__link" href="">My Trips</a>
     
    </nav>
  </div>
  <main class="mdl-layout__content">
      <div style = "position:relative; left:50px; top:50px; ">
    <h3>      SET A NEW TRIP</h3>
    
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 50%; width : 50% }
    </style>
          
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css' rel='stylesheet'/>
  </head>
  <body>
  <body>
      
      
      <h5>
          Please enter the trip name and country you are visiting :
        <br><div class="mdl-textfield mdl-js-textfield">
                       <input class="mdl-textfield__input" type="text" id="name">
                           <label class="mdl-textfield__label" for="sample1">Enter Trip Name </label>
                    </div>
          <br><body id="departureCountry">
            <br><select id="selectDepartureCountry">
                <option>select your country</option>
            </select>
              <button onclick="searchDepartureAirport()"class="mdl-button mdl-js-button mdl-button--raised">Enter
           
          </button>
          
      
       <div>
           <br>
           
          <select name="addDestinationAirPort" id="addDestinationAirPort" >
              
           <option>select your starting Airport</option>
          </select>
           <button onclick="enterNew()"> enter</button><br><br>
      
        <div id = "add"style="font-size:10px;"></div>
      <button onclick="addanAirport()"class="mdl-button mdl-js-button mdl-button--raised">Add a Stop</button>
          <button onclick = "confirmTrip()"class="mdl-button mdl-js-button mdl-button--raised">Confirm Stops</button>
           
    
          
 <!-- List with avatar and controls -->
<br><br>

         
           
    
      
     
      
    
      
    
      
      <input type="date" id="star" name="calendar" value=""><br>
      <input type="date" id="end" name="calendar" value=""><br><br>
      
      
     <button onclick='save()'>Save this Trip</button>
      
      

      
    <div id='map'></div>
    <script>
    
        // This should be your own API key
        mapboxgl.accessToken = 'pk.eyJ1IjoiaGFveHVhbnRhbyIsImEiOiJja29vN2hidnMwM3l2Mm5xa3pkOHlmNWs4In0.mULwk1FSGAkZ0d4M7HLMPg';
    
        let caulfield = [145.0420733, -37.8770097];
    
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: caulfield
        });


        // This function checks whether there is a map layer with id matching 
        // idToRemove.  If there is, it is removed.
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        }

    </script>
          

        
    
          
      </div>
  </main>
</div>
    </body>
<script src="js/shared.js"></script>
    <script src="js/NewTrip1.js"></script>
</html>