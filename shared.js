const TRIP_INDEX_KEY = "tripIndex";
const TRIP_DATA_KEY = "TripData"
const USER_DATA_KEY = "UserData"


class User{
    constructor(Name,Password){
        this._name = Name;
        this._password = Password;
    }
    get name(){
        return this._name
    }
    get password(){
        return this._password
    }
    fromData(data){
        this._name=data._name
    this._password=data._password
        
    }
}
    
class Trip{
  constructor(id){
    this._id=id
    this._name=""
    this._username=""
    this._country=""
    this._date=[]
    this._airports=[]
    this._distance = " "
    this._password=""
  }
  get id(){
        return this._id;
     }
  get country(){
    return this._country 
  }
  get date(){
    return this._date
  }
  get airports(){
    return this._airports
  }
  get name(){
    return this._name
  }
  get distance(){
    return this._distance
  }
   set country(text){
     this._country=text 
  }
  set date(date){
     this._date=date 
  }
  
  set name(text){
    this._name=text
  }
  set distance(number){
    this._distance=number
  }
  addAirports(){
      
  }
  removeAirports(){
    
  }
  getStartPoint(){
    return this._airports[0]
  }
  getEndPoint(){
    let end_idx=this._airports.length-1
   return this._airports[end_idx]
   
  }
  getTotalStops(){
     return this._airports.length
    
  }
   fromData(data){
    this._country=data._country
    this._date=data._date
    this._airports=data._airports
    this._distance =data._distance
    this._name=data._name
    this._id=data.id
  }
}
class TripList{
    constructor(){
        this._trips=[]     
    }
    addTrip(id){
          let addedTrip = new Trip(id);
        this._trips.push(addedTrip);
    }
    removeTrip(id) {
           for (let i = 0; i < this._trips.length; i++){
            if (this._trips[i].id == id){
                this._trips.splice(i, 1);
            }
        }        
    }
     fromData(data){
        let array = data._trips;
        this._trips = [];
        for (let i = 0; i < array.length; i++)
        {
            let newTrip = new Trip();
            newTrip.fromData(array[i]);
            this._trips.push(newTrip);
        }
    }
}
function checkIfDataExistsLocalStorage(){
   let dataExist = localStorage.getItem(TRIP_DATA_KEY);
   if (!dataExist) {
       return false;
   }
   else{
    if (typeof dataExist == null){
           return false;
        }
    else if (typeof dataExist == undefined){
           return false;
        }
    else if (dataExist == "") {
       return false;
       }
     else
       {
        return true;
       }
   }
}
let trips1= new TripList()
 function updateLocalStorage(data){
   let jsonString = JSON.stringify(data);
   localStorage.setItem(TRIP_DATA_KEY, jsonString);
}

if (checkIfUSERDataExistsLocalStorage() == false){
    let user= new User("New","0000")
    window.location = "SignIN.html";
    updateUSERLocalStorage(user)
    
}
function getDataLocalStorage()
{
   let jsonString = localStorage.getItem(TRIP_DATA_KEY);
return JSON.parse(jsonString);
}

function checkIfUSERDataExistsLocalStorage(){
   let dataExist = localStorage.getItem(USER_DATA_KEY);
   if (!dataExist) {
       return false;
   }
   else{
    if (typeof dataExist == null){
           return false;
        }
    else if (typeof dataExist == undefined){
           return false;
        }
    else if (dataExist == "") {
       return false;
       }
     else
       {
        return true;
       }
   }
}
 function updateUSERLocalStorage(data){
   let jsonString = JSON.stringify(data);
   localStorage.setItem(USER_DATA_KEY, jsonString);
}

